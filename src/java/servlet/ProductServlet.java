/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import com.data.koneksi;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fajar mustaqin
 */
public class ProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            RequestDispatcher dispatch = request.getRequestDispatcher("index.jsp");
            dispatch.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            koneksi kon = new koneksi();
            //Connection cn = kon.masuk();
            
            String awal = request.getParameter("first_name");
            String akhir = request.getParameter("last_name");
            String email = request.getParameter("Email");
            String telepon = request.getParameter("Telepon");
            String facebook = request.getParameter("fb");
            String awlsewa = request.getParameter("Awal_sewa");
            String jamsewa = request.getParameter("Jam_sewa");
            String menitsewa = request.getParameter("Menit_sewa");
            String akhrsewa = request.getParameter("Akhir_sewa");
            String jamkembali = request.getParameter("Jam_kembali");
            String menitkembali = request.getParameter("Menit_kembali");
            String ket = request.getParameter("Keterangan");
            
            
            
            
            Statement st = cn.createStatement();
            ResultSet rs;
            int i = st.executeUpdate("insert into tb_rental(NamaDp,NamaBk,Email,Telepon,"
                    + "Fb_Twet,Awal_sewa,Jam_Sewa,Menit_Sewa,Akhir_Sewa,Jam_Kembali,Menit_Kembali,"
                    + "Ket)values('"+awal+"','"+akhir+"','"+email+"',"
                            + "'"+telepon+"','"+facebook+"','"+awlsewa+"','"+jamsewa+"',"
                                    + "'"+menitsewa+"','"+akhrsewa+"','"+jamkembali+"',"
                                            + "'"+menitkembali+"','"+ket+"')");
            
            processRequest(request, response);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
