/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.data;

/**
 *
 * @author fajar mustaqin
 */
public class product {

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public void setTelepon(String Telepon) {
        this.Telepon = Telepon;
    }

    public void setFb(String fb) {
        this.fb = fb;
    }

    public void setAwal_sewa(String awal_sewa) {
        this.awal_sewa = awal_sewa;
    }

    public void setJam_sewa(String jam_sewa) {
        this.jam_sewa = jam_sewa;
    }

    public void setMenit_sewa(String menit_sewa) {
        this.menit_sewa = menit_sewa;
    }

    public void setAkhir_sewa(String akhir_sewa) {
        this.akhir_sewa = akhir_sewa;
    }

    public void setJam_kembali(String jam_kembali) {
        this.jam_kembali = jam_kembali;
    }

    public void setMenit_kembali(String menit_kembali) {
        this.menit_kembali = menit_kembali;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return Email;
    }

    public String getTelepon() {
        return Telepon;
    }

    public String getFb() {
        return fb;
    }

    public String getAwal_sewa() {
        return awal_sewa;
    }

    public String getJam_sewa() {
        return jam_sewa;
    }

    public String getMenit_sewa() {
        return menit_sewa;
    }

    public String getAkhir_sewa() {
        return akhir_sewa;
    }

    public String getJam_kembali() {
        return jam_kembali;
    }

    public String getMenit_kembali() {
        return menit_kembali;
    }

    public String getKeterangan() {
        return keterangan;
    }
    
    private String first_name;
    private String last_name;
    private String Email;
    private String Telepon;
    private String fb;
    private String awal_sewa;
    private String jam_sewa;
    private String menit_sewa;
    private String akhir_sewa;
    private String jam_kembali;
    private String menit_kembali;
    private String keterangan;
}
