<%-- 
    Document   : index
    Created on : Dec 13, 2021, 11:41:00 AM
    Author     : fajar mustaqin
--%>

<%@page import="java.sql.Connection"%>
<%@page import="com.data.koneksi"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <title>Rental Motor Online</title>
    </head>
    <body>
        <div>
        <input type="submit" style="background-color:cornflowerblue" value="Reservasi online rental motor Pamulang" >
        <form action="ProductServlet" method="POST">
            <main>
            <table>
                <tr>
                    <td>
                        <label>Nama Lengkap *</label>
                    </td>
                    <td><input class="firstname" type="text" name="first_name"></td> 
<!--                    <label class="firstlabel"><i><small>First Name</small></i></label>-->
                    <td>
                        
                        <input class="lastname" type="text" name="last_name">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Alamat Email *</label>
                    </td>
                    <td>
                        <input class="email" type="email" name="Email"><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Telepon *</label>
                    </td>
                    <td>
                        <input type="text" class="telepon" name="Telepon"><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Facebook/Twiteer</label>
                    </td>
                    <td>
                        <input class="fb" type="text" name="fb">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Awal Sewa *</label>
                    </td>
                    <td>
                        <input class="awalsewa" name="awal_sewa" type="date">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Jam Sewa *</label>
                    </td>
                    <td>
                        <input class="jam" name="jam_sewa" type="text">
                    </td>
                    <td>
                        <input class="menit" name="menit_sewa" type="text">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Akhir Sewa *</label>
                    </td>
                    <td>
                        <input type="date" class="akhirsewa" name="akhir_sewa">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Jam Kembali *</label>
                    </td>
                    <td>
                        <input type="text" class="jamkembali" name="jam_kembali">
                    </td>
                    <td>
                        <input type="text" class="menitkembali" name="menit_kembali">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Keterangan *</label>
                    </td>
                    <td>
                        <input type="textarea" class="keterangan" name="keterangan" >
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button name="Reservasi" class="reservasi" type="submit">Reservasi</button>
                        <button name="Batal">Batal</button>
                    </td>
                </tr>
            </table>
                <%
            
        %>
        <br>
        <table>
            <thead>
                <tr>
                    <td>nama awal</td>
                    <td>nama akhir</td>
                    <td>email</td>
                    <td>telepon</td>
                    <td>facebook</td>
                    <td>awal sewa</td>
                    <td>jam sewa</td>
                    <td>menit sewa</td>
                    <td>akhir sewa</td>
                    <td>jam sewa</td>
                    <td>menit sewa</td>
                    <td>keterangan</td>
            </thead>
            <tbody>
                
        </form>
        </main>
        </div>
    </body>
</html>
